<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    // Welcome
    public function home(){
        return view('welcome');
    }

    // About
    public function about(){
        return view('pages/about');
    }
}
